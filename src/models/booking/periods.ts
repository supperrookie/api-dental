import { Knex } from 'knex';

export class PeriodsModel {

  list(db: Knex) {
    return db('periods');
  }  

  getByID(db: Knex, id: number) {
    return db('periods')
    .where('period_id', id);
  }  

  getByHospitalID(db: Knex, id: number) {
    return db('periods')
    .where('hospital_id', id);
  }  

  getSearch(db: Knex, text: string) {
    return db('periods')
    .whereLike('period_name', text);
  }  

  create(db: Knex, data: any) {
    return db('periods')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('periods')
    .where('period_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('periods')
      .where('period_id', id)
      .delete();
  }

}