import S from 'fluent-json-schema'

const schema = S.object()
  .prop('an', S.string().maxLength(15).required())
  .prop('hn', S.string().maxLength(15).required())
  .prop('vn', S.string().maxLength(15).required())
  .prop('insurance_id', S.string().format('uuid').required())
  .prop('department_id', S.string().format('uuid').required())
  .prop('pre_diag', S.string().maxLength(250).required())
  .prop('ward_id', S.string().format('uuid').required())
  .prop('bed_id', S.string().format('uuid'))
  .prop('admit_date', S.string().format('date').required())
  .prop('admit_time', S.string().format('time').required())
  .prop('doctor_id', S.string().format('uuid'))
  .prop('admit_by', S.string().format('uuid'))

export default {
  body: schema
}